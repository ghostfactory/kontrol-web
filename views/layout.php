<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <title>My App</title>

    <!-- Styles -->
    <?php loadCSS(array(
        "//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css",
        "http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,100italic,300italic,400italic,500italic,700italic,",
        "assets/fonts/museo-sans-rounded/font.css"
    )); ?>


</head>
<body>
    <div class="statusbar-overlay"></div>

    <div class="views">
        <div class="view view-main">

            <div class="navbar"></div>

            <div class="pages navbar-through">
                <div class="page"></div>
            </div>

        </div>
    </div>

    <!-- Scripts -->
    <?php loadScripts(array(
        "https://cdn.socket.io/socket.io-1.2.1.js",
        "//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js",
    )); ?>

    <!-- App -->
    <?php loadApp(); ?>
</body>
</html>