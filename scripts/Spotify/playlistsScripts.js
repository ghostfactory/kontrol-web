app.onRouteLoad('Spotify:playlists', function (routeData)
{
    $('.pull-to-refresh-content').on('refresh', function(e) {
        app.refreshResource('Spotify.playlists', function() {
            app.fw7.pullToRefreshDone();
        });
    });
});