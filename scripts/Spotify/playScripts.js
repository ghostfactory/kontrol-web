app.onRouteLoad('Spotify:play', function (routeData)
{
    var uri = routeData.args[0];
    var track_num = routeData.args[1];

    $.post(app.config.api + "/mopidy/playback/play/uri", {
        uri: decodeURIComponent(uri),
        at_position: track_num
    }, function(response) {

    });

});