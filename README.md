Kontrol Web
============

*This program is currently not set up to be easily cloned an ran, as there are some hard-coded values specific to my setup, such as the API path. If you decide to play around with the program, some tinkering will be needed to make it work correctly. I will try to document everything when I can.*

Kontrol is a work-in-progress suite of applications that provide multiple different home-automation services. Kontrol Web is a web application client to access and consume the Kontrol API. It is built with [Framework7](http://www.idangero.us/framework7/) with a custom-tailored MVC framework built around it.

Current Services
------------------------
* Spotify
* Super basic LED control