var gulp = require('gulp');
var sass = require('gulp-sass');

var paths = {
    sass: {
        src: './assets/scss/*.scss',
        dest: './assets/css'
    }
};

// default gulp task
gulp.task('default', ['sass'], function() {
    // Watch SASS
    gulp.watch(paths.sass.src, ['sass']);
});


// SASS
gulp.task('sass', function () {
    gulp.src(paths.sass.src)
        .pipe(sass())
        .pipe(gulp.dest(paths.sass.dest));
});