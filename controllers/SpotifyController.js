var SpotifyController = app.controller('Spotify');

SpotifyController.action('playlists', 'views/Spotify/playlistsPage.html', function($scope) {
    $scope.playlists = app.repositories['SpotifyPlaylist'].getPlaylists();

    return $scope;
});

SpotifyController.action('playlist', 'views/Spotify/playlistPage.html', function($scope, uri) {
    $scope.playlist = app.repositories['SpotifyPlaylist'].getPlaylist(uri);

    return $scope;
});

SpotifyController.action('play', 'views/Spotify/playPage.html', function($scope, uri, track_num) {
    $scope.uri = uri;
    $scope.trackNum = track_num;
    $scope.track = app.repositories['SpotifyPlaylist'].getPlaylist(uri).tracks[track_num]

    return $scope;
});