app.router.setRoutes({
    '/':    'Main:index',

    'spotify':                                  'Spotify:playlists',
    'spotify/playlist/:uri':                    'Spotify:playlist',
    'spotify/play/:uri/track_num/:track_num':   'Spotify:play',

    'raspi':   'Raspi:index'
});