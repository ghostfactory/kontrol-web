var SpotifyPlaylistModel = app.model('SpotifyPlaylist', function(data) {
    var model = this;

    if(data == null || typeof data == 'undefined') {
        return null;
    }

    model.name = data.name;
    model.fullName = "";
    model.uri = data.uri;
    model.folders = [];
    model.tracks = [];
    for(var i in data.tracks) {
        var trackData = data.tracks[i];
        var track = new app.models['SpotifyTrack'](trackData);
        model.tracks.push(track);
    }

    var folderParts = model.name.split("/");
    for(var i in folderParts) {
        var part = folderParts[i];
        if(i < folderParts - 1) {
            model.folders.push(part);
        } else {
            model.name = part;
        }

        model.fullName += (i == folderParts.length - 1) ? part : "[" + part + "] ";
    }

    return model;
});