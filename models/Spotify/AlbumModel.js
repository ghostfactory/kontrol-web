var SpotifyAlbumModel = app.model('SpotifyAlbum', function(data) {
    var model = this;

    if(data == null || typeof data == 'undefined') {
        return null;
    }

    model.name = data.name;
    model.uri = data.uri;
    model.date = data.date;
    model.artists = [];
    for(var i in data.artists) {
        var artistData = data.artists[i];
        var artist = new app.models['SpotifyArtist'](artistData);
        model.artists.push(artist);
    }

    return model;
});