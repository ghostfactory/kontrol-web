var SpotifyArtistModel = app.model('SpotifyArtist', function(data) {
    var model = this;

    if(data == null || typeof data == 'undefined') {
        return null;
    }

    model.name = data.name;
    model.uri = data.uri;

    return model;
});