var SpotifyTrackModel = app.model('SpotifyTrack', function(data) {
    var model = this;

    if(data == null || typeof data == 'undefined') {
        return null;
    }

    model.name = data.name;
    model.uri = data.uri;
    model.length = data.length;
    model.trackNum = data.track_no;
    model.date = data.date;
    model.bitrate = data.bitrate;
    model.album = new app.models['SpotifyAlbum'](data.album);
    model.artists = [];
    model.artistsString = "";
    for(var i in data.artists) {
        var artistData = data.artists[i];
        var artist = new app.models['SpotifyArtist'](artistData);
        model.artists.push(artist);

        model.artistsString += artist.name;
        if(i < data.artists.length - 1) {
            model.artistsString += ", ";
        }
    }
    model.playlistUri = null;
    model.playlistNum = null;

    return model;
});