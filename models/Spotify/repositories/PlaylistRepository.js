var SpotifyPlaylistRepository = SpotifyPlaylistModel.repository;

SpotifyPlaylistRepository.loadPlaylists = function(successCallback, errorCallback) {
    $.get(app.config.api + "/mopidy/playlists").then(function(response) {
        var playlists = [];

        for(var i in response.data) {
            var data = response.data[i];
            if(data.uri.indexOf('spotify') != 0) { // Skip non-spotify playlist
                continue;
            }

            var playlist = new app.models['SpotifyPlaylist'](data);

            for(var j in playlist.tracks) {
                var track = playlist.tracks[j];
                track.playlistUri = playlist.uri;
                track.playlistNum = j;
            }

            playlists.push(playlist);
        }

        successCallback(playlists);
    }, errorCallback);
}

SpotifyPlaylistRepository.loadPlaylist = function(uri, successCallback, errorCallback) {
    $.get(app.config.api + "/mopidy/playlists/lookup", {
        uri: decodeURIComponent(uri)
    }).then(function(response) {
        var data = response.data;

        if(data.uri.indexOf('spotify') != 0) { // Non-spotify playlist
            return null;
        }

        var playlist = new app.models['SpotifyPlaylist'](data);

        successCallback(playlist);
    }, errorCallback);
}

SpotifyPlaylistRepository.getPlaylists = function(url) {
    if(!app.container.Spotify.playlists) {
        return null;
    }

    return app.container.Spotify.playlists;
}

SpotifyPlaylistRepository.getPlaylist = function(uri) {
    if(!app.container.Spotify.playlists) {
        return null;
    }

    uri = decodeURIComponent(uri);

    for(var i in app.container.Spotify.playlists) {
        var playlist = app.container.Spotify.playlists[i];
        if(playlist.uri == uri) {
            return playlist;
        }
    }

    return null;
}