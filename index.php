<?php

require_once('views/layout.php');

function loadViews() {
    foreach(glob("views/*/_layout.html") as $file) {
        require_once($file);
    }
}

function loadApp() {
    $appScripts = array(
        'config',
        'models/*',
        'models/*/repositories',
        'controllers',
        'loaders',
        'scripts/*'
//        'directives',
//        'factories',
    );

    $html = "";
    $html .= '<script> window.app = new MVC7(); </script>';
    $html .= '<script src="app.js"></script>';

    foreach($appScripts as $folder) {
        foreach(glob($folder . "/*.js") as $file) {
            $html .= '<script src="' . $file . '"></script>' . "\n";
        }
    }

    $html .= '<script> app.init(); </script>';

    echo $html;
}

function loadScripts($scripts = array()) {
    $html = "";

    foreach($scripts as $file) {
        $html .= '<script src="' . $file . '"></script>' . "\n";
    }

    foreach(glob("assets/js/lib/*.js") as $file) {
        $html .= '<script src="' . $file . '"></script>' . "\n";
    }

    foreach(glob("assets/js/*.js") as $file) {
        $html .= '<script src="' . $file . '"></script>' . "\n";
    }

    echo $html;
}

function loadCSS($styles = array()) {
    $html = "";

    foreach($styles as $file) {
        $html .= '<link rel="stylesheet" href="' . $file . '">' . "\n";
    }

    foreach(glob("assets/css/lib/*.css") as $file) {
        $html .= '<link rel="stylesheet" href="' . $file . '">' . "\n";
    }

    foreach(glob("assets/css/*.css") as $file) {
        $html .= '<link rel="stylesheet" href="' . $file . '">' . "\n";
    }

    echo $html;
}