(function () {
    'use strict';

    window.$$ = Dom7;

    /* ===========================
     MVC7
     =========================== */
    window.MVC7 = function()
    {
        var mvc7 = this;

        mvc7.fw7 = new Framework7({
            cache: false,
            preloadPreviousPage: false,
            swipeBackPage: false,
            animateNavBackIcon: true,
            template7Pages: false
        });

        // Version
        mvc7.version = '0.0.1';

        // Default Parameters
        mvc7.params = {
            urlPrefix: '#/',
            controllerActionDelimeter: ':',
            defaultRoute: '/',
            loadingRoute: 'loading',
            refreshContentSelector: '.refresh-content',
            useTemplate7: true
        };

        // Binding
        mvc7.rivetBinder = null;

        // MVC
        mvc7.models = {};
        mvc7.controllers = {};
        mvc7.repositories = {};

        // Events
        mvc7.events = {
            onRouteLoad: []
        }

        // Currents
        mvc7.mainView = null;
        mvc7.currentRouteData = {
            controller: null,
            action: null,
            args: null,
            context: null
        };

        // History
        mvc7.history = {
            routePaths: []
        }

        // Container
        mvc7.container = {};
        mvc7.resources = []; // EX: [{path: "Test.testing", loader: (a function), finished: false}]


        /* ================================
         Init
         ================================== */
        mvc7.init = function()
        {
            // Add views
            $.each($$(document).find('.view'), function(i, view) {
                var checkView = mvc7.fw7.addView($(view), {
                    dynamicNavbar: true
                });

                if($(view).hasClass('view-main')) {
                    mvc7.mainView = checkView;
                }
            });

            // Init router
            mvc7.router.init();

            // Load initial page
            var routeToLoad = mvc7.params.defaultRoute;
            if(mvc7.resources.length > 0) {
                routeToLoad = mvc7.params.loadingRoute;
            }
            mvc7.router.loadRoute(routeToLoad, {
                animatePages: false
            });

            // Load resources
            mvc7.loadAllResources();
        }


        /* ================================
         Set Params
         ================================== */
        mvc7.setParams = function(params)
        {
            for (var param in params) {
                mvc7.params[param] = params[param];
            }
        }


        /* ================================
         Container
         ================================== */
        mvc7.addResource = function(path, loader)
        {
            mvc7.resources.push({
                path: path,
                loader: loader,
                finished: false
            });
        }

        mvc7.loadAllResources = function()
        {
            if(mvc7.resources.length == 0) {
                mvc7.resourcesLoaded();
            }

            for(var i in mvc7.resources) {
                var resource = mvc7.resources[i];

                resource.loader(resource, function (resource, data) {
                    setValue(mvc7.container, resource.path, data);
                    resource.finished = true;
                    mvc7.checkResourcesLoaded();
                });
            }
        }

        mvc7.loadResource = function(path, callback)
        {
            if(mvc7.resources.length == 0) {
                callback();
            }

            for(var i in mvc7.resources) {
                var resource = mvc7.resources[i];

                if(resource.path != path) {
                    continue;
                }

                resource.finished = false;

                return resource.loader(resource, function (resource, data) {
                    setValue(mvc7.container, resource.path, data);
                    resource.finished = true;
                    callback(data);
                });
            }
        }

        mvc7.resourcesLoaded = function()
        {
            for(var i in mvc7.resources) {
                var loader = mvc7.resources[i];
                if(loader.finished === false) {
                    return false;
                }
            }

            return true;
        }

        mvc7.checkResourcesLoaded = function()
        {
            if(mvc7.resourcesLoaded()) {
                return mvc7.finishedLoading();
            }
        }

        mvc7.finishedLoading = function()
        {
            mvc7.router.loadRoute(mvc7.params.defaultRoute);
        }


        /* ================================
         Template Binding
         ================================== */
        mvc7.bindTemplate = function(template, context)
        {
            if(mvc7.params.useTemplate7) {
                var compiledTemplate = Template7.compile(template.html());
                var $compiledTemplate = $(compiledTemplate(context));
                template.html($compiledTemplate);
                mvc7.fw7.initPullToRefresh($compiledTemplate);
                return $compiledTemplate;
            }

            return template;
        }

        mvc7.getActionData = function(action, args)
        {
            var actionFuncString = "action.callback({}";
            for(var i in args) {
                actionFuncString += ", args[" + i + "]";
            }
            actionFuncString += ");";

            return eval(actionFuncString);
        }

        mvc7.refreshResource = function(path, callback)
        {
            //callback();
            app.loadResource(path, function(data) {
                var tempData = mvc7.router.getRouteDataFromRoute(mvc7.currentRouteData.controller.name + ":" + mvc7.currentRouteData.action.name, mvc7.currentRouteData.args);
                mvc7.currentRouteData.context = tempData.context;
                var template = app.bindTemplate($(mvc7.currentRouteData.template).find(mvc7.params.refreshContentSelector), mvc7.currentRouteData.context);
                $('.page-on-center').find(mvc7.params.refreshContentSelector).html(template);
                callback();
            });
        }


        /* ================================
         Routing
         ================================== */
        mvc7.router = {
            routes: {
                '/': 'Main:index',
                'loading': 'Main:loading'
            },

            init: function() {
                var router = this;

                // On Page Before Init
                mvc7.fw7.onPageBeforeInit('*', function() {
                    // Template binding
                    // On regular: page-on-right
                    // On back: page-on-center

                    var $content = $('.page-on-right');
                    if(mvc7.goingBack) {
                        $content = $(".page-on-center");
                    }

                    $content.attr('data-page', mvc7.currentRouteData.controller.name + ':' + mvc7.currentRouteData.action.name);
                });

                // On Page Init
                mvc7.fw7.onPageInit('*', function() {

                });

                // On Page Before Animation
                mvc7.fw7.onPageBeforeAnimation('*', function () {
                    // Template binding
                    // On regular: page-on-right
                    // On back: page-on-left
                    var $content = $(".page-on-right");
                    if(mvc7.goingBack) {
                        $content = $(".page-on-left");
                        mvc7.goingBack = false;
                    }
                    mvc7.currentRouteData.template = $content.html();
                    mvc7.bindTemplate($content, mvc7.currentRouteData.context);

                    // Events
                    mvc7.callEvent('onRouteLoad', mvc7.currentRouteData.controller.name + ":" + mvc7.currentRouteData.action.name);

                    // Load route on click
                    $$(".page").on('click', 'a', function () {
                        var href = $(this).attr('href');

                        if (href == mvc7.params.urlPrefix) {
                            return router.loadRoute('/');
                        }

                        if (href.substring(0, 2) == "#/") {
                            var routePath = href.substring(2, href.length);
                            return router.loadRoute(routePath);
                        }
                    });
                });

                // On Page Back
                mvc7.fw7.onPageBack('*', function () {
                    mvc7.goingBack = true;
                    mvc7.history.routePaths.pop();
                    var routePath = mvc7.history.routePaths[mvc7.history.routePaths.length - 1];
                    mvc7.currentRouteData = mvc7.router.getRouteDataFromRoutePath(routePath);
                });
            },

            setRoutes: function(routes)
            {
                for (var route in routes) {
                    this.routes[route] = routes[route];
                }
            },

            loadRoute: function(routePath, options)
            {
                mvc7.history.routePaths.push(routePath);
                mvc7.currentRouteData = this.getRouteDataFromRoutePath(routePath);

                var params = {
                    url: mvc7.currentRouteData.action.viewUrl,
                    context: mvc7.currentRouteData.context
                };

                for(var i in options) {
                    params[i] = options[i];
                }

                return mvc7.fw7.mainView.router.load(params);
            },

            getRouteDataFromRoutePath: function(checkRoutePath)
            {
                // EX route: #/playlist/spotify:palylist:adW83na1337
                // EX routes: 'playlist/:uri' : 'Spotify:playlist

                var checkRouteParts = checkRoutePath.split('/');
                var theRoute = null;
                var theRoutePath = null;
                var theRouteParts = [];
                var args = [];

                for(var routePath in mvc7.router.routes) {
                    var route = this.routes[routePath];
                    var routeParts = routePath.split('/');

                    if(routeParts.length != checkRouteParts.length) {
                        continue;
                    }

                    var check = true;
                    for(var j in routeParts) {
                        var part = routeParts[j];
                        var checkPart = checkRouteParts[j];

                        if(part.indexOf(':') == 0) {
                            continue;
                        }

                        if(part !== checkPart) {
                            check = false;
                            break;
                        }
                    }

                    if(check == true) {
                        theRoute = route;
                        theRoutePath = routePath;
                        theRouteParts = routeParts;
                        break;
                    }
                }

                if(theRoute == null || theRoutePath == null || theRouteParts.length == 0) {
                    return console.log("ERROR: There is no route defined for '" + checkRoutePath + "'.");
                }

                // GET ARGS
                for(var i in theRouteParts) {
                    var part = theRouteParts[i];
                    if(part.indexOf(':') == 0) {
                        args.push(checkRouteParts[i]);
                    }
                }

                return this.getRouteDataFromRoute(theRoute, args);
            },

            getRouteDataFromRoute: function(route, args)
            {
                var routePair = route.split(mvc7.params.controllerActionDelimeter);
                var controller = app.controllers[routePair[0]];
                var action = controller.actions[routePair[1]];
                var context = mvc7.getActionData(action, args);

                return {
                    controller: controller,
                    action: action,
                    args: args,
                    context: context
                };
            }

        }


        /* ================================
         Controllers
         ================================== */
        mvc7.controller = function(controllerName)
        {
            var controller = {
                name: controllerName
            };

            // Actions
            controller.actions = {};
            controller.action = function(actionName, viewUrl, callback)
            {
                var action = {
                    name: actionName,
                    viewUrl: viewUrl,
                    callback: callback
                };

                controller.actions[actionName] = action;
                return action;
            }

            // Return
            mvc7.controllers[controllerName] = controller;
            return controller;
        }


        /* ================================
         Models
         ================================== */
        mvc7.model = function(modelName, modelFunction)
        {
            var model = {};

            // Repository
            model.repository = {};

            // Return
            mvc7.models[modelName] = modelFunction;
            mvc7.repositories[modelName] = model.repository;
            return model;
        }


        /* ================================
         Events
         ================================== */
        mvc7.callEvent = function(event, channel) {
            for(var i in mvc7.events[event]) {
                var eventObject = mvc7.events[event][i];
                if(eventObject.channel == channel || eventObject.channel == '*') {
                    eventObject.callback(mvc7.currentRouteData);
                }
            }
        }

        mvc7.onRouteLoad = function(channel, callback)
        {
            mvc7.events.onRouteLoad.push({
                channel: channel,
                callback: callback
            })
        }

    }

})();